#!/bin/bash

set -e

echo -e "\nInstalling NPM dependencies for agent\n"
npm install --force

echo -e "\nInstalling NPM dependencies for drivers\n"
for f in `ls -1 drivers`;
do
  cd drivers/$f
  npm install --force
  cd ../.. 
done

echo -e "\nAll done!\n"
