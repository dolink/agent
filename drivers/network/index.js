var util = require('util');
var stream = require('stream');
var os = require('os');

function Network(opts, app) {
  this.config = function (rpc, cb) {
    cb(null, os.networkInterfaces());
  };
}

util.inherits(Network, stream);

module.exports = Network;
